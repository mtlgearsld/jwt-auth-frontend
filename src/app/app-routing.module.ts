import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SignupFormComponent } from "./signup-form/signup-form.component";
import { LoginFormComponent } from "./login-form/login-form.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";

const routes: Routes = [
  { path: "", component: SignupFormComponent },
  { path: "login", component: LoginFormComponent },
  {
    path: "landingPage",
    component: LandingPageComponent,
  },
  { path: "**", redirectTo: "" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
