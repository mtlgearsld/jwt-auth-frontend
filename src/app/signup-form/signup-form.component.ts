import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { SignupServiceService } from "./signup-service.service";

@Component({
  selector: "app-signup-form",
  templateUrl: "./signup-form.component.html",
  styleUrls: ["./signup-form.component.scss"],
})
export class SignupFormComponent implements OnInit {
  signUpForm = new FormGroup({
    name: new FormControl(""),
    email: new FormControl(""),
    password: new FormControl(""),
    passwordConfirm: new FormControl(""),
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private users: SignupServiceService
  ) {}

  onSubmit() {
    console.log(this.signUpForm.value);

    this.users
      .create(this.signUpForm.value)
      .subscribe(() => this.router.navigateByUrl("/login"));
  }

  ngOnInit() {}
}
