import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

const apiUrl = (path: string) => `http://localhost:3000/${path}`;

@Injectable({
  providedIn: "root",
})
export class SignupServiceService {
  constructor(private readonly http: HttpClient) {}

  create(user: any) {
    return this.http.post(apiUrl(`users/signup/`), user);
  }
}
