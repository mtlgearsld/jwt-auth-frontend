import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { tap } from "rxjs/operators";

interface UserResponse {
  user: {
    _id: string;
    name: string;
    token: string;
  };
}

@Injectable({ providedIn: "root" })
export class AuthService {
  user: {
    _id: string;
    name: string;
    token: string;
  };
  url = (path: string) => `http://localhost:3000/${path}`;
  httpOptions = () => ({
    headers: new HttpHeaders({ "x-access-token": this.user.token }),
  });

  constructor(private http: HttpClient) {}

  login(credentials: { email: string; password: string }) {
    console.log({ credentials });

    return this.http
      .post<UserResponse>(this.url("users/login/"), credentials)
      .pipe(
        tap((res: UserResponse) => {
          localStorage.setItem("user", JSON.stringify(res.user));
          // JSON.parse(localStorage.getItem('user')) use to get stored info as json
          this.user = res.user;
        })
      );
  }

  genericRequest() {
    return this.http.get(this.url("/current"), this.httpOptions());
  }

  logOut() {
    // localStorage.removeItem("user") use when you logOut & set user = {}
    localStorage.removeItem("user");
    this.user = { _id: null, name: null, token: null };
  }
}
