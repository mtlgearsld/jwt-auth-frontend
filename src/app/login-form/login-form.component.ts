import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { LogInService } from "./log-in.service";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"],
})
export class LoginFormComponent implements OnInit {
  logInForm = new FormGroup({
    email: new FormControl(""),
    password: new FormControl(""),
  });

  get email() {
    return this.logInForm.get("email");
  }

  get password() {
    return this.logInForm.get("password");
  }

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private users: LogInService
  ) {}

  onSubmit() {
    console.log(this.logInForm.value);

    this.users
      .create(this.logInForm.value)
      .subscribe(() => this.router.navigateByUrl("/landingPage"));
  }

  ngOnInit() {}
}
