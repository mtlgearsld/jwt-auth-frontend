import { environment as dev } from "./environment";

export const environment = {
  ...dev, // an object spread to get everything from env.ts in here
  production: true,
};
